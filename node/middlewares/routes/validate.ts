import { UserInputError } from '@vtex/api'

import { IOServiceError } from '../../utils/errorHandling/ioServiceError'

export async function validate(ctx: RouteContext, next: () => Promise<void>) {
  try {
    const {
      vtex: {
        route: { params },
      },
    } = ctx

    ctx.set('Cache-Control', 'no-cache no-store')

    const { code } = params

    if (!code) {
      throw new UserInputError('Code is required')
    }

    const codeNumber = parseInt(code as string, 10)

    if (Number.isNaN(codeNumber)) {
      throw new UserInputError('Code must be a number')
    }

    ctx.state.code = codeNumber

    await next()
  } catch (error) {
    if (error instanceof IOServiceError) {
      throw error
    }

    throw new IOServiceError({
      message: 'Error validating code',
      status: 400,
      reason: error.message,
      code: 'USER_INPUT_ERROR',
      date: new Date(),
      exception: error,
    })
  }
}

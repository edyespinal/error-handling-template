import { IOServiceError } from '../../utils/errorHandling/ioServiceError'

export async function getStatus(ctx: RouteContext) {
  try {
    const {
      state: { code },
      clients: { status: statusClient },
    } = ctx

    const statusResponse = await statusClient.getStatus(code)

    if (!statusResponse) {
      throw new Error('No response')
    }

    ctx.body = statusResponse
  } catch (error) {
    throw new IOServiceError({
      message: 'Error getting status',
      status: error.response?.status || 400,
      reason: error.message,
      code: 'NOT_FOUND_ERROR',
      date: new Date(),
      exception: error,
    })
  }
}

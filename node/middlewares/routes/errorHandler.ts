import { IOServiceLogger } from '../../utils/errorHandling/ioServiceLogger'

export async function errorHandler(
  ctx: RouteContext,
  next: () => Promise<void>
) {
  try {
    ctx.state.serviceLogger = new IOServiceLogger(ctx)
    await next()
  } catch (error) {
    ctx.state.serviceLogger.error(error)

    ctx.status = error.status
    ctx.body = {
      message: error.message,
      status: error.status,
      reason: error.reason,
      code: error.code,
    }
  }
}

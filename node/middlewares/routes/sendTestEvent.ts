import { IOServiceError } from '../../utils/errorHandling/ioServiceError'

export async function sendTestEvent(ctx: RouteContext) {
  try {
    const {
      clients: { events },
    } = ctx

    events.sendEvent('', 'test-event', { message: 'Test event fired' })

    ctx.body = {
      code: 200,
      message: 'Test event fired',
    }
  } catch (error) {
    throw new IOServiceError({
      message: 'Error firing test event',
      status: 500,
      reason: error.message,
      code: 'INTERNAL_SERVER_ERROR',
      date: new Date(),
      exception: error,
    })
  }
}

import { IOServiceError } from '../../utils/errorHandling/ioServiceError'
import {
  SERVICE_LOGS_BUCKET,
  SERVICE_LOGS,
} from '../../utils/errorHandling/constants'

export async function getAppLogs(ctx: RouteContext) {
  try {
    const {
      clients: { vbase },
    } = ctx

    const errorLogs = await vbase.getJSON<IOServiceError[]>(
      SERVICE_LOGS_BUCKET,
      SERVICE_LOGS,
      true
    )

    if (!errorLogs) {
      throw new Error('No error logs found')
    }

    ctx.body = errorLogs
  } catch (error) {
    throw new IOServiceError({
      message: 'Error getting error logs',
      status: error.response?.status || 404,
      reason: error.message,
      code: 'NOT_FOUND_ERROR',
      date: new Date(),
      exception: error,
    })
  }
}

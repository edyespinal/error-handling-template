import { backgroundProcess } from '../../services'
import { IOServiceError } from '../../utils/errorHandling/ioServiceError'

export async function onTestEvent(ctx: EventContext) {
  try {
    const {
      body,
      sender,
      state: { appSettings, serviceLogger },
    } = ctx

    if (appSettings.debugMode) {
      serviceLogger.info({
        message: 'Test event received',
        body,
      })
    }

    backgroundProcess(ctx, sender)
  } catch (error) {
    throw new IOServiceError({
      message: 'Error firing test event',
      status: 500,
      reason: error.message,
      code: 'INTERNAL_SERVER_ERROR',
      date: new Date(),
      exception: error,
    })
  }
}

import { getAppSettings } from '../getAppSettings'
import { onErrorHandler } from './onErrorHandler'
import { onTestEvent } from './onTestEvent'

export const applicationEvents = {
  testEvent: [onErrorHandler, getAppSettings, onTestEvent],
}

import type {
  IOServiceErrorArgs,
  IOServiceErrorCodes,
} from '../../typings/errorHandling/ioServiceLogger'

export class IOServiceError extends Error {
  public status: number
  public reason: string
  public code: IOServiceErrorCodes[keyof IOServiceErrorCodes]
  public date: Date
  public exception: Error

  constructor(args: IOServiceErrorArgs) {
    const { message, status, reason, code, date, exception } = args

    super(message)
    this.name = 'ServiceError'
    this.status = status
    this.reason = reason
    this.code = code
    this.date = date
    this.exception = exception
  }
}

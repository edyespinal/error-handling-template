export const SERVICE_LOGS_BUCKET = 'service-error-bucket'
export const SERVICE_LOGS = 'service-error-logs'
export const LOGS_MAX_SIZE = 10000
export const DEBUG_MODE = 'debug-mode'

export type StatusResponse = {
  code: number
  description: string
}

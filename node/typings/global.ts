import type {
  EventContext as EventIOContext,
  RecorderState,
  ServiceContext,
} from '@vtex/api'

import type { Clients } from '../clients'
import type { AppSettings } from './appSettings'
import type { IOServiceLogger } from '../utils/errorHandling/ioServiceLogger'

declare global {
  type RouteContext = ServiceContext<Clients, State>

  type EventContext = EventIOContext<Clients, State>

  interface State extends RecorderState {
    code: number
    appSettings: AppSettings
    serviceLogger: IOServiceLogger
  }
}

import type { IOServiceError } from '../../utils/errorHandling/ioServiceError'

export type IOServiceErrorArgs = {
  message: string
  status: number
  reason: string
  code: IOServiceErrorCodes[keyof IOServiceErrorCodes]
  date: Date
  exception: Error
}

export type IOServiceErrorCodes = {
  AUTHENTICATION_ERROR: 'AUTHENTICATION_ERROR'
  FORBIDDEN_ERROR: 'FORBIDDEN_ERROR'
  NOT_FOUND_ERROR: 'NOT_FOUND_ERROR'
  RESOLVER_ERROR: 'RESOLVER_ERROR'
  RESOLVER_WARNING: 'RESOLVER_WARNING'
  USER_INPUT_ERROR: 'USER_INPUT_ERROR'
  INTERNAL_SERVER_ERROR: 'INTERNAL_SERVER_ERROR'
}

export type InfoLog = Record<string, unknown>

export type IOServiceLogs = Array<IOServiceError | InfoLog>

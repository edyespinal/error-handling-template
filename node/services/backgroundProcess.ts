import { IOServiceError } from '../utils/errorHandling/ioServiceError'

export async function backgroundProcess(
  ctx: RouteContext | EventContext,
  sender: string
) {
  const {
    state: { appSettings, serviceLogger },
  } = ctx

  try {
    await new Promise((resolve) => setTimeout(resolve, 1000))

    const random = Math.random()

    if (appSettings.debugMode) {
      serviceLogger.info({
        message: `Random number is ${random}`,
      })
    }

    if (random < 0.5) {
      throw new Error('Random number is less than 0.5')
    }

    if (appSettings.debugMode) {
      serviceLogger.info({
        message: `Background process finished for sender ${sender} after 10 seconds`,
      })
    }
  } catch (error) {
    const serviceError = new IOServiceError({
      message: 'Error in background process',
      status: 500,
      reason: error.message,
      code: 'INTERNAL_SERVER_ERROR',
      date: new Date(),
      exception: error,
    })

    serviceLogger.error(serviceError)
  }
}

# VTEX IO Error Handling Guidelines

Error handling guidelines for VTEX IO Services.


## Table of contents

1. [Introduction](#introduction)
2. [Error types](#error-types)
   - [Client and user input error (4xx)](#expected-errors-client-and-user-input-errors-4xx)
   - [Server-side and API errors (5xx)](#unexpected-errors-server-side-and-api-errors-5xx)
   - [Error structure](#error-structure)
   - [Debug mode](#debug-mode)
   - [Accesing application logs](#access-application-logs)
3. [Best practices](#best-practices)
   - [Error handler middleware](#error-handling-middleware)
   - [Background processes](#background-processes)



## Introduction

Error handling is an important part of developing VTEX IO services. It helps to ensure that the service is reliable and can continue to operate even when errors occur. This documentation provides guidelines and best practices for handling errors in VTEX IO services.
The main objectives of these guidelines are to:

- Ensure a seamless user experience by avoiding complete disruptions of the service.
- Communicate and log error messages clearly.
- Speed up debugging and maintenance for the service.

## Error types

Our service can experience a variety of errors. To simplify error handling and improve our team's ability to debug issues, we can classify errors into two main categories:

- Expected errors in the flow of our service (client and user errors (4xx))
- Unexpected errors (server-side and API errors (5xx))

By classifying errors in this way, we can develop more targeted and effective error handling strategies. For example, we can return informative error messages to users for client and user input errors, and we can log server-side and API errors to OpenSearch for investigation and resolution.
In addition to simplifying error handling, this classification can also help to improve our team's ability to debug issues. When a team member is debugging an issue, they can quickly focus on the relevant category of errors to narrow down the possible causes. This can lead to faster and more efficient debugging.

### Expected errors (Client and user input errors (4xx))

Client and user input errors are the types of errors that are expected to occur in the flow of your services, such as missing settings. These errors should be handled to avoid disruptions in our service and informative error messages should be returned and logged.

To log client and user input errors, you can use the following VBase bucket and path:

```
bucket:  service-logs-bucket
path:    service-logs
```

The shape, grouping and maximum number of records to be stored in this file will depend on your service, but it is recommended to keep it at 1000 records. You may also want to add informative logs to this file to help you understand the errors better.

### Unexpected errors (Server-side and API errors (5xx))

Server-side and API errors are the types of errors that are unexpected in the flow of your application. These errors can be caused by a variety of factors, such as timeouts, database errors, network errors, or internal server errors.
It is recommended to log server-side and API errors to OpenSearch (or your own logging system) so that they can be investigated and resolved.
In addition to logging server-side and API errors, you may also want to consider sending notifications to your team when these errors occur. This will help you to quickly identify and resolve critical errors.

### Error structure

This is the recommended structure for the errors to be logged:

```
message: string
status: number (http status code)
reason: string
code: string
date: Date
exception: Error
```

Example:

```
{
   "message": "Unable to get application settings",
   "status": 401,
   "reason": "Credentials were not provided",
   "code": "AUTHENTICATION_ERROR",
   "exception": {{Error object}}
}
```

### Debug mode

It is recommended that you add a `debugMode` property to your application settings.
The debug mode could enable your service to log more information on certain processes to make the debugging process easier.
For example, if your service processes multiple products you can create a record when this process begins and another when the process ends. If the debug mode is enabled, your service could log information about each product's individual process.

### Access application logs

It is recommended that you create an endpoint to access the logs stored on `VBase` that can be accessed by someone with the necessary credentials.

Add this endpoint to the `service.json` file on your project:

```
"routes: {
   .
   .
   .
   "appLogs": {
      "path": "/_v/name-of-your-application/app-logs",
      "public": false
   }
}
```

This will create the follwing endpoint:

```
public:  https://{workspace}--{account}.myvtex.com/_v/name-of-your-application/app-logs
private: https://app.io.vtex.com/{vendor.application-name}/v0/{account}/{workspace}/_v/name-of-your-application/app-logs
```

_**Note:** the endpoint `public` property is set to false to keep the endpoint private and secure. If you choose to keep the endpoint as public (true) it is highly encouraged that you set an `authentication` middleware to ensure that only people with the necessary credentials can access the logs._

## Best Practices

### Error handling middleware

Given that VTEX uses KoaJS for the services, we can take advantage of Koa's middleware Promise chain and include an `errorHandler` at the top (beginning of the array) of each endpoint (routes and events) of the service.
This will ensure that any errors that occur during the request to the endpoint are "catch" by our error handler if we implement a `try/catch` on each of our middlewares.

### Background processes

When running background processes it is important to remember that the `errorHandler` middleware will no longer be able to `catch` an error since the Promise chain might be resolved already.
In these cases you can wrap the entire background process in a `try/catch` block and handle the error here.
